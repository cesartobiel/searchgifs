package com.cesarcardenas.giphysearch.data.services.giphy

data class FixedHeightSmall(
    val url: String
)