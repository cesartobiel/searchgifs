package com.cesarcardenas.giphysearch.data.services.giphy

data class GiphyResponse(
    val `data`: List<Data>,
    val pagination: Pagination
)

data class Pagination(
    val count: Int,
    val offset: Int,
    val total_count: Int
)
