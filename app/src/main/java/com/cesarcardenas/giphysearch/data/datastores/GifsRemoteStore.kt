package com.cesarcardenas.giphysearch.data.datastores

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.cesarcardenas.giphysearch.data.services.NoInternetConnection
import com.cesarcardenas.giphysearch.data.services.RequestResult
import com.cesarcardenas.giphysearch.data.services.giphy.GiphyService
import com.cesarcardenas.giphysearch.data.services.giphy.GiphyResponse
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

interface GifsRemoteStore{
    fun getGifsWith(name:String): LiveData<RequestResult>
}

class GiphyRemoteStore(private val giphyService: GiphyService) : GifsRemoteStore{

    //TODO see if I want to allow more parameters
    override fun getGifsWith(name: String) : LiveData<RequestResult> {
        val result = MutableLiveData<RequestResult>()

        giphyService.search(query = name).enqueue(object: Callback<GiphyResponse>{
            override fun onResponse(call: Call<GiphyResponse>, response: Response<GiphyResponse>) {
                if(response.isSuccessful) {
                    result.value = RequestResult.Success(response.body()?.data ?: emptyList())
                } else {
                    result.value = RequestResult.Error(response.code(), response.errorBody()?.string())
                }
            }

            override fun onFailure(call: Call<GiphyResponse>, t: Throwable) {
                result.value = RequestResult.Error(NoInternetConnection, t.localizedMessage)
            }

        })
        return result
    }
}