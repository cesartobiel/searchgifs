package com.cesarcardenas.giphysearch.data.repositories

import androidx.lifecycle.LiveData
import com.cesarcardenas.giphysearch.data.datastores.GifsRemoteStore
import com.cesarcardenas.giphysearch.data.services.RequestResult
import com.cesarcardenas.giphysearch.data.services.giphy.Data
import com.cesarcardenas.giphysearch.domain.GifsRepository
import com.cesarcardenas.giphysearch.domain.entities.GifEntitiy
import com.cesarcardenas.giphysearch.domain.entities.GifsResult
import com.snakydesign.livedataextensions.map

class RemoteGifsRepository(private val gifsRemoteStore : GifsRemoteStore) : GifsRepository{

    override fun getGifsWith(name: String): LiveData<GifsResult> {
        return gifsRemoteStore.getGifsWith(name).map {
            val gifs = mutableListOf<GifEntitiy>()
            when(it){
               is RequestResult.Success -> {
                   it.gifsData.forEach { gifData ->
                       gifs.add(gifFromData(gifData))
                   }
                   GifsResult.Succesfull(gifs)
               }
               is RequestResult.Error -> {
                   GifsResult.Error(it.code, it.errorMessage)
               }

            }

        }
    }

    //TODO move to a mapper object
    private fun gifFromData(data: Data): GifEntitiy {
        return GifEntitiy(data.title,data.images.fixed_height_downsampled.url,data.images.fixed_height_small.url)
    }

}