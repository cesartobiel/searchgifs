package com.cesarcardenas.giphysearch.data.services.giphy

import com.cesarcardenas.giphysearch.BuildConfig
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface GiphyService{

    @GET("gifs/search")
    fun search(@Query("api_key") apiKey:String = BuildConfig.GiphyApiKey,
               @Query("limit") limit:String = defaultLimit,
               @Query("q") query:String,
               @Query("offset") offset:Int = defaultoffset,
               @Query("rating") rating:String = defaultRating,
               @Query("lang") language:String = defaultLanguage
    ) : Call<GiphyResponse>
}

val defaultLimit: String get() = "100"
val defaultoffset: Int get() = 0
val defaultRating: String get() = "G"
val defaultLanguage: String get() = "en"