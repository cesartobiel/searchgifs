package com.cesarcardenas.giphysearch.data.services.giphy

data class FixedHeightDownSampled(
    val url: String
)