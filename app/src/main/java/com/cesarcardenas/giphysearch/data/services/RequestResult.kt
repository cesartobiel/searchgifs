package com.cesarcardenas.giphysearch.data.services

import com.cesarcardenas.giphysearch.data.services.giphy.Data

sealed class RequestResult{
    data class Success(val gifsData: List<Data>) : RequestResult()
    data class Error(val code: Int, val errorMessage:String?) :RequestResult()
}