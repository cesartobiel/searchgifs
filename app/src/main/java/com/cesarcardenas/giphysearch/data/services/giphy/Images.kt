package com.cesarcardenas.giphysearch.data.services.giphy

data class Images(
    val fixed_height_downsampled: FixedHeightDownSampled,
    val fixed_height_small: FixedHeightSmall
)