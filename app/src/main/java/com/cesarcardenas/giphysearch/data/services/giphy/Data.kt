package com.cesarcardenas.giphysearch.data.services.giphy

data class Data(
    val id: String,
    val title: String,
    val images: Images)