package com.cesarcardenas.giphysearch.presentation.di.modules

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.cesarcardenas.giphysearch.presentation.di.keys.ViewModelKey
import com.cesarcardenas.giphysearch.presentation.di.scopes.ApplicationScope
import com.cesarcardenas.giphysearch.presentation.gifsearch.GifsSearchViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap
import javax.inject.Inject
import javax.inject.Provider
import javax.inject.Singleton

@Module
abstract class ViewModelsModule{

    @Binds
    @IntoMap
    @ViewModelKey(GifsSearchViewModel::class)
    abstract fun bindsGifsSearchViewModel(gifsSearchViewModel: GifsSearchViewModel) : ViewModel

    @Binds
    @ApplicationScope
    abstract fun bindViewModelFactory( factory: ViewModelFactory): ViewModelProvider.Factory
}

@Suppress("UNCHECKED_CAST")
class ViewModelFactory @Inject
constructor(private val creators: Map<Class<out ViewModel>, @JvmSuppressWildcards Provider<ViewModel>>) : ViewModelProvider.Factory {

    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        var creator: Provider<out ViewModel>? = creators[modelClass]
        if (creator == null) {
            for ((key, value) in creators) {
                if (modelClass.isAssignableFrom(key)) {
                    creator = value
                    break
                }
            }
        }
        if (creator == null) {
            throw IllegalArgumentException("unknown model class " + modelClass)
        }
        try {
            return creator.get() as T
        } catch (e: Exception) {
            throw RuntimeException(e)
        }

    }
}