package com.cesarcardenas.giphysearch.presentation.di.modules

import com.cesarcardenas.giphysearch.data.datastores.GifsRemoteStore
import com.cesarcardenas.giphysearch.data.repositories.RemoteGifsRepository
import com.cesarcardenas.giphysearch.domain.GifsRepository
import com.cesarcardenas.giphysearch.presentation.di.scopes.ApplicationScope
import dagger.Module
import dagger.Provides

@Module(includes = [DataStoresModule::class])
class RepositoriesModule{

    @Provides
    @ApplicationScope
    fun providesGifsRepository(gifsRemoteStore: GifsRemoteStore) : GifsRepository{
        return RemoteGifsRepository(gifsRemoteStore)
    }
}