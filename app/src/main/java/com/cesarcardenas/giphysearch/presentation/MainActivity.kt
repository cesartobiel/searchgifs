package com.cesarcardenas.giphysearch.presentation

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.cesarcardenas.giphysearch.R
import com.cesarcardenas.giphysearch.presentation.gifsearch.GifsSearchFragment
import dagger.android.DaggerActivity
import dagger.android.HasActivityInjector
import dagger.android.support.DaggerAppCompatActivity
import dagger.android.support.HasSupportFragmentInjector

class MainActivity : DaggerAppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.main_activity)
        if (savedInstanceState == null) {
            supportFragmentManager.beginTransaction()
                .replace(R.id.container, GifsSearchFragment.newInstance())
                .commitNow()
        }
    }

}
