package com.cesarcardenas.giphysearch.presentation.di.modules

import com.cesarcardenas.giphysearch.data.datastores.GifsRemoteStore
import com.cesarcardenas.giphysearch.data.datastores.GiphyRemoteStore
import com.cesarcardenas.giphysearch.data.services.giphy.GiphyService
import com.cesarcardenas.giphysearch.presentation.di.scopes.ApplicationScope
import dagger.Module
import dagger.Provides

@Module(includes = [ApiClientsModule::class])
class DataStoresModule{

    @Provides
    @ApplicationScope
    fun providesGifsRemoteStore(giphyService: GiphyService): GifsRemoteStore{
        return GiphyRemoteStore(giphyService)
    }
}