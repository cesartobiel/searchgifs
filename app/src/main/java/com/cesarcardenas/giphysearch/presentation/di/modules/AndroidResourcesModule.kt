package com.cesarcardenas.giphysearch.presentation.di.modules

import android.content.Context
import com.cesarcardenas.giphysearch.presentation.AndroidPlatformResources
import com.cesarcardenas.giphysearch.presentation.PlatformResources
import com.cesarcardenas.giphysearch.presentation.di.scopes.ApplicationScope
import dagger.Module
import dagger.Provides

@Module
class AndroidResourcesModule(){

    @Provides
    @ApplicationScope
    fun providesPlatformResources(context: Context): PlatformResources {
        return AndroidPlatformResources(context)
    }
}