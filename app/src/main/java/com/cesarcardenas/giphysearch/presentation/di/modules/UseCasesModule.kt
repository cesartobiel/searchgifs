package com.cesarcardenas.giphysearch.presentation.di.modules

import com.cesarcardenas.giphysearch.domain.GifsRepository
import com.cesarcardenas.giphysearch.domain.usecases.SearchGifsUseCase
import dagger.Module
import dagger.Provides

@Module(includes = [RepositoriesModule::class])
class UseCasesModule{

    @Provides
    fun provicesSearchGifsUseCase(gifsRepository: GifsRepository): SearchGifsUseCase{
        return SearchGifsUseCase(gifsRepository)
    }
}