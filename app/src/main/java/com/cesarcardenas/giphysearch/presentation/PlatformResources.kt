package com.cesarcardenas.giphysearch.presentation

import android.content.Context
import androidx.annotation.StringRes

interface PlatformResources{
    fun getString(@StringRes id: Int): String
}

class AndroidPlatformResources(private val context: Context): PlatformResources{
    override fun getString(id: Int): String {
        return context.getString(id)
    }

}