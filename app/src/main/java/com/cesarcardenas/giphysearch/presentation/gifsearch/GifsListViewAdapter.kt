package com.cesarcardenas.giphysearch.presentation.gifsearch

import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.cesarcardenas.giphysearch.presentation.entitites.Gif
import android.view.LayoutInflater
import android.view.View
import android.widget.ImageView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions.withCrossFade
import com.cesarcardenas.giphysearch.R
import kotlinx.android.synthetic.main.layout_gif_grid_layout_item.view.*


class GifsListViewAdapter(gifs : List<Gif>) : RecyclerView.Adapter<GifsListViewAdapter.GifListGridItemViewHolder>(){

    val gifsList = gifs.toMutableList()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): GifListGridItemViewHolder {
        val mView = LayoutInflater.from(parent.context).inflate(R.layout.layout_gif_grid_layout_item, parent, false)
        return GifListGridItemViewHolder(mView)
    }

    override fun getItemCount(): Int {
        return gifsList.size
    }

    override fun onBindViewHolder(holder: GifListGridItemViewHolder, position: Int) {
        holder.updateView(gifsList[holder.adapterPosition])
    }

    fun replaceGifs(newGifs: List<Gif>){
        gifsList.clear()
        gifsList.addAll(newGifs)
    }

    class GifListGridItemViewHolder(mView: View) : RecyclerView.ViewHolder(mView) {
        val gifImage: ImageView = mView.gifImage

        fun updateView(gif: Gif){
            //Load image with glide.
            //Todo Create  image loader interface
            Glide.with(gifImage)
                .load(gif.url)
//                .placeholder(R.mipmap.ic_launcher_round)
//                .transition(withCrossFade())
                .into(gifImage)
        }
    }

}


