package com.cesarcardenas.giphysearch.presentation.di.modules

import com.cesarcardenas.giphysearch.data.services.giphy.GiphyService
import com.cesarcardenas.giphysearch.presentation.di.scopes.ApplicationScope
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit


@Module(includes = [NetworkModule::class])
class ApiClientsModule{

    @Provides
    @ApplicationScope
    fun provideGiphyService(retrofitBuilder: Retrofit.Builder): GiphyService {
      return retrofitBuilder
            .baseUrl("https://api.giphy.com/v1/")
            .build()
            .create(GiphyService::class.java)
    }
}