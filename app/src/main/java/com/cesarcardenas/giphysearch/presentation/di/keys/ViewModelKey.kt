package com.cesarcardenas.giphysearch.presentation.di.keys

import androidx.lifecycle.ViewModel
import dagger.MapKey

import java.lang.annotation.Retention
import java.lang.annotation.RetentionPolicy
import kotlin.annotation.MustBeDocumented
import kotlin.reflect.KClass


@MustBeDocumented
@Target(AnnotationTarget.FUNCTION, AnnotationTarget.PROPERTY_GETTER, AnnotationTarget.PROPERTY_SETTER)
@Retention(RetentionPolicy.RUNTIME)
@MapKey
internal annotation class ViewModelKey(val value: KClass<out ViewModel>)