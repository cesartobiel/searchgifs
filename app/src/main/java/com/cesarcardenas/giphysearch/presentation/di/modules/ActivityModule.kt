package com.cesarcardenas.giphysearch.presentation.di.modules

import com.cesarcardenas.giphysearch.presentation.MainActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ActivityModule{

    @ContributesAndroidInjector(modules = [FragmentsModule::class])
    abstract fun mainActivity(): MainActivity
}