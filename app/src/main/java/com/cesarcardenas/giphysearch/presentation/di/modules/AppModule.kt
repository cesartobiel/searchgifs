package com.cesarcardenas.giphysearch.presentation.di.modules

import android.app.Application
import android.content.Context
import com.cesarcardenas.giphysearch.R
import dagger.Binds
import dagger.Module
import dagger.Provides

@Module(includes = [AndroidResourcesModule::class])
abstract class AppModule{
    @Binds
    abstract fun bindContext(application: Application): Context
}