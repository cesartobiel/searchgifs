package com.cesarcardenas.giphysearch.presentation.di.components

import android.app.Application
import com.cesarcardenas.giphysearch.presentation.GifsSearchApp
import com.cesarcardenas.giphysearch.presentation.di.modules.*
import com.cesarcardenas.giphysearch.presentation.di.scopes.ApplicationScope
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjector
import dagger.android.support.AndroidSupportInjectionModule

@ApplicationScope
@Component(modules = [
        AndroidSupportInjectionModule::class,
        AppModule::class,
        UseCasesModule::class,
        ActivityModule::class,
        ViewModelsModule::class
    ]
)
interface AppComponent: AndroidInjector<GifsSearchApp>{

    @Component.Builder
    interface Builder {
        @BindsInstance fun application(application: Application): Builder
        fun build(): AppComponent
    }

}