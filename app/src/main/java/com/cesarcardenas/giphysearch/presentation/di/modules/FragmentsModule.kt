package com.cesarcardenas.giphysearch.presentation.di.modules

import com.cesarcardenas.giphysearch.presentation.gifsearch.GifsSearchFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class FragmentsModule{

    @ContributesAndroidInjector
    abstract fun gifsSearchFragment(): GifsSearchFragment
}