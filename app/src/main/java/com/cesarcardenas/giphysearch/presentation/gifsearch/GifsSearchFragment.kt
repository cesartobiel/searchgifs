package com.cesarcardenas.giphysearch.presentation.gifsearch

import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.SearchView
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.cesarcardenas.giphysearch.R
import com.cesarcardenas.giphysearch.presentation.entitites.Gif
import dagger.android.support.DaggerFragment
import kotlinx.android.synthetic.main.gifs_search_fragment.*
import javax.inject.Inject
import android.content.res.Configuration
import android.widget.Toast


class GifsSearchFragment : DaggerFragment(), SearchView.OnQueryTextListener {

    companion object {
        fun newInstance() = GifsSearchFragment()
    }

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory
    lateinit var gifsListViewAdapter: GifsListViewAdapter

    private lateinit var viewModel: GifsSearchViewModel

    //region Lifecycle
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        val view =  inflater.inflate(R.layout.gifs_search_fragment, container, false)

        val list = view.findViewById<RecyclerView>(
            com.cesarcardenas.giphysearch.R.id.gifsList)
        var gridColumns = 3
        if (resources.configuration.orientation == Configuration.ORIENTATION_LANDSCAPE) gridColumns = 5

        list.layoutManager = GridLayoutManager(context,gridColumns)
        gifsListViewAdapter = GifsListViewAdapter(emptyList())
        list.adapter = gifsListViewAdapter

        viewModel = ViewModelProviders.of(this,viewModelFactory).get(GifsSearchViewModel::class.java)

        viewModel.getViewState().observe(viewLifecycleOwner, Observer { handleViewState(it) })


        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        searchView.setOnQueryTextListener(this)
    }

    //endregion

    //region Update Screen
    private fun handleViewState(state: SearchGifsViewState) {
        //Update title
        requireActivity().title = state.title

        //Update images
        gifsListViewAdapter.replaceGifs(state.gifs)
        gifsListViewAdapter.notifyDataSetChanged()

        //Display errors
        state.errorState?.let {
            Toast.makeText(context,it.message,Toast.LENGTH_LONG).show()
        }

    }
    //endregion

    override fun onQueryTextSubmit(query: String?): Boolean {
        query?.let{
            viewModel.onUiEvent(SearchGifViewUIEvent.Search(query))
        }
        return false
    }

    override fun onQueryTextChange(newText: String?): Boolean {
        return false
    }

}
