package com.cesarcardenas.giphysearch.presentation.entitites

data class Gif(val title:String, val url: String)