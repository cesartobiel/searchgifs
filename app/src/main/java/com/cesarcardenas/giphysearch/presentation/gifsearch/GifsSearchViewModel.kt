package com.cesarcardenas.giphysearch.presentation.gifsearch

import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.cesarcardenas.giphysearch.R
import com.cesarcardenas.giphysearch.data.services.NoInternetConnection
import com.cesarcardenas.giphysearch.domain.entities.GifEntitiy
import com.cesarcardenas.giphysearch.domain.entities.GifsResult
import com.cesarcardenas.giphysearch.domain.usecases.SearchGifsUseCase
import com.cesarcardenas.giphysearch.presentation.PlatformResources
import com.cesarcardenas.giphysearch.presentation.entitites.Gif
import com.snakydesign.livedataextensions.switchMap
import javax.inject.Inject

class GifsSearchViewModel @Inject constructor(
    private val searchGifsUseCase: SearchGifsUseCase,
    private val platformResources: PlatformResources
) : ViewModel() {

    private val viewState = MediatorLiveData<SearchGifsViewState>()
    private val searchTrigger = MutableLiveData<String>()

    private val gifs = searchTrigger.switchMap {
        searchGifsUseCase.search(it)
    }

    init {
        viewState.value = SearchGifsViewState(platformResources.getString(R.string.searchgifs_view_title), emptyList(),null)
        listenToDataStreams()
    }

    private fun listenToDataStreams() {
        viewState.addSource(gifs){
            when(it){
                is GifsResult.Succesfull -> {
                    handleGifsResultSuccess(it)
                }
                is GifsResult.Error -> {
                    handleGifsResultError(it)
                }
            }

        }
    }

    private fun handleGifsResultError(it: GifsResult.Error) {
        val errorMessage: String = when (it.code) {
            NoInternetConnection -> platformResources.getString(R.string.no_internet)
            else -> platformResources.getString(R.string.try_again)
        }

        this.viewState.value =
            viewState.value?.copy(errorState = ErrorState(errorMessage))
    }

    private fun handleGifsResultSuccess(it: GifsResult.Succesfull) {
        if (it.gifs.isNotEmpty()) {
            this.viewState.value =
                viewState.value?.copy(gifs = it.gifs.map { gifEntity ->
                    mapGifEntityToGif(gifEntity)
                }, errorState = null)
        } else {
            this.viewState.value =
                viewState.value?.copy(errorState = ErrorState(platformResources.getString(R.string.no_results)))
        }
    }

    //todo move to mapper
    private fun mapGifEntityToGif(gifEntity: GifEntitiy): Gif {
        return Gif(
            gifEntity.name,
            gifEntity.smallSizeUrl
        )
    }

    fun onUiEvent(event: SearchGifViewUIEvent){
        when(event){
            is SearchGifViewUIEvent.Search -> handleSearch(event.searchTerm)
        }
    }

    private fun handleSearch(searchTerm: String) {
        this.searchTrigger.value = searchTerm
    }

    fun getViewState(): LiveData<SearchGifsViewState> {
        return this.viewState
    }

}

data class SearchGifsViewState(
    val title: String,
    val gifs:List<Gif>,
    val errorState: ErrorState?
)

data class ErrorState(val message : String)

sealed class SearchGifViewUIEvent{
    data class Search(val searchTerm: String): SearchGifViewUIEvent()
}
