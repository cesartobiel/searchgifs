package com.cesarcardenas.giphysearch.domain.entities

sealed class GifsResult{
    data class Succesfull(val gifs:List<GifEntitiy>) : GifsResult()
    data class Error(val code:Int, val message:String?) : GifsResult()
}