package com.cesarcardenas.giphysearch.domain.entities

data class GifEntitiy(val name:String, val downSampledUrl:String, val smallSizeUrl:String)
