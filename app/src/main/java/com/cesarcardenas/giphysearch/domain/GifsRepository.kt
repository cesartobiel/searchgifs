package com.cesarcardenas.giphysearch.domain

import androidx.lifecycle.LiveData
import com.cesarcardenas.giphysearch.domain.entities.GifEntitiy
import com.cesarcardenas.giphysearch.domain.entities.GifsResult

interface GifsRepository{
    fun getGifsWith(name:String) : LiveData<GifsResult>
}