package com.cesarcardenas.giphysearch.domain.usecases

import androidx.lifecycle.LiveData
import com.cesarcardenas.giphysearch.domain.GifsRepository
import com.cesarcardenas.giphysearch.domain.entities.GifEntitiy
import com.cesarcardenas.giphysearch.domain.entities.GifsResult

class SearchGifsUseCase(private val gifsRepository: GifsRepository){

    fun search(query : String): LiveData<GifsResult>{
        return gifsRepository.getGifsWith(query)
    }
}