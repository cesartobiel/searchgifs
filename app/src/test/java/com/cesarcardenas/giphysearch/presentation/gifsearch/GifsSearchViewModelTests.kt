package com.cesarcardenas.giphysearch.presentation.gifsearch

import com.cesarcardenas.giphysearch.R
import com.cesarcardenas.giphysearch.common.LiveDataTest
import com.cesarcardenas.giphysearch.common.MockitoTest
import com.cesarcardenas.giphysearch.common.generateGifEntities
import com.cesarcardenas.giphysearch.data.services.NoInternetConnection
import com.cesarcardenas.giphysearch.domain.GifsRepository
import com.cesarcardenas.giphysearch.domain.entities.GifsResult
import com.cesarcardenas.giphysearch.domain.usecases.SearchGifsUseCase
import com.cesarcardenas.giphysearch.presentation.PlatformResources
import com.jraska.livedata.test
import com.snakydesign.livedataextensions.liveDataOf
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito.*

class GifsSearchViewModelTests: LiveDataTest, MockitoTest{

    lateinit var searchGifsUseCase: SearchGifsUseCase
    @Mock lateinit var gifsRepository: GifsRepository
    @Mock lateinit var platformResources: PlatformResources
    lateinit var gifsSearchViewModel: GifsSearchViewModel

    @Before
    fun setup(){
        searchGifsUseCase = SearchGifsUseCase(gifsRepository)
        `when`(platformResources.getString(anyInt())).thenReturn("something")
        gifsSearchViewModel = GifsSearchViewModel(searchGifsUseCase, platformResources)
    }

    @Test
    fun initialState(){
        //Act
        val testObserver = gifsSearchViewModel.getViewState().test()

        //Assert
        val state = testObserver.assertHistorySize(1).value() as SearchGifsViewState
        Assert.assertNotNull(state.title)
        Assert.assertEquals(0,state.gifs.size)
    }

    @Test
    fun gifsResultSuccessful_WithGifs_SendsStateWithGifs(){
        //Arrange
        val searchTerm = "heroes"
        val expectedNumberOfGifs = 2
        `when`(gifsRepository.getGifsWith(searchTerm))
            .thenReturn(liveDataOf(GifsResult.Succesfull(generateGifEntities(expectedNumberOfGifs))))
        val testObserver = gifsSearchViewModel.getViewState().test()

        //Act
        gifsSearchViewModel.onUiEvent(SearchGifViewUIEvent.Search(searchTerm))

        //Assert
        val state = testObserver.assertHistorySize(2).value() as SearchGifsViewState
        Assert.assertNotNull(state.title)
        Assert.assertEquals(expectedNumberOfGifs,state.gifs.size)
        Assert.assertNull(state.errorState)
    }

    @Test
    fun gifsResultSuccessful_NoGifs_SendsErrorState(){
        //Arrange
        val searchTerm = "heroes"
        `when`(gifsRepository.getGifsWith(searchTerm))
            .thenReturn(liveDataOf(GifsResult.Succesfull(listOf())))

        val testObserver = gifsSearchViewModel.getViewState().test()

        //Act
        gifsSearchViewModel.onUiEvent(SearchGifViewUIEvent.Search(searchTerm))

        //Assert
        val state = testObserver.assertHistorySize(2).value() as SearchGifsViewState
        Assert.assertNotNull(state.title)
        Assert.assertNotNull(state.errorState)
        verify(platformResources, times(1)).getString(R.string.no_results)
    }

    @Test
    fun gifsResultError_InternetIssues_SendsErrorState(){
        //Arrange
        val searchTerm = "heroes"
        `when`(gifsRepository.getGifsWith(searchTerm))
            .thenReturn(liveDataOf(GifsResult.Error(NoInternetConnection,"")))

        val testObserver = gifsSearchViewModel.getViewState().test()

        //Act
        gifsSearchViewModel.onUiEvent(SearchGifViewUIEvent.Search(searchTerm))

        //Assert
        val state = testObserver.assertHistorySize(2).value() as SearchGifsViewState
        Assert.assertNotNull(state.title)
        Assert.assertNotNull(state.errorState)
        verify(platformResources, times(1)).getString(R.string.no_internet)
    }

    @Test
    fun gifsResultError_ServerErrors_SendsErrorState(){
        //Arrange
        val searchTerm = "heroes"
        `when`(gifsRepository.getGifsWith(searchTerm))
            .thenReturn(liveDataOf(GifsResult.Error(403,"Not authorized")))

        val testObserver = gifsSearchViewModel.getViewState().test()

        //Act
        gifsSearchViewModel.onUiEvent(SearchGifViewUIEvent.Search(searchTerm))

        //Assert
        val state = testObserver.assertHistorySize(2).value() as SearchGifsViewState
        Assert.assertNotNull(state.title)
        Assert.assertNotNull(state.errorState)
        verify(platformResources, times(1)).getString(R.string.try_again)
    }

    @Test
    fun successfulSearch_WithGifs_ClearsErrorState(){
        //Arrange
        val failSearch = "will fail"
        val successfulSearch = "will pass"
        `when`(gifsRepository.getGifsWith(failSearch))
            .thenReturn(liveDataOf(GifsResult.Error(403,"Not authorized")))

        `when`(gifsRepository.getGifsWith(successfulSearch))
            .thenReturn(liveDataOf(GifsResult.Succesfull(generateGifEntities(10))))

        val testObserver = gifsSearchViewModel.getViewState().test()

        //Act
        gifsSearchViewModel.onUiEvent(SearchGifViewUIEvent.Search(failSearch))
        gifsSearchViewModel.onUiEvent(SearchGifViewUIEvent.Search(successfulSearch))

        //Assert
        val state = testObserver.assertHistorySize(3).value() as SearchGifsViewState
        Assert.assertNotNull(state.title)
        Assert.assertNull(state.errorState)
    }
}