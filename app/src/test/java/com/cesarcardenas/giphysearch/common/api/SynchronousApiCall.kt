package com.cesarcardenas.giphysearch.common.api

import okhttp3.Request
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.IOException

abstract class SynchronousApiCall<T> constructor(protected val response: T) : Call<T>{

     override fun enqueue(callback: Callback<T>) {
        synchronousEnqueue(callback)
     }

    abstract fun synchronousEnqueue(callback: Callback<T>)

     override fun isExecuted(): Boolean {
         TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
     }

     override fun clone(): Call<T> {
         TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
     }

     override fun isCanceled(): Boolean {
         TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
     }

     override fun cancel() {
         TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
     }

     override fun execute(): Response<T> {
         TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
     }

     override fun request(): Request {
         TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
     }

 }

class SuccessFulApiCall<T>constructor(response: T) : SynchronousApiCall<T>(response){

    override fun synchronousEnqueue(callback: Callback<T>) {
        callback.onResponse(this, getSuccessfulResponse())
    }

    private fun getSuccessfulResponse(): Response<T> {
        return Response.success(response)
    }
}

class FaileApiCall<T>constructor(response: T,
                                 val isNetworkError:Boolean,
                                 val errorCode : Int = 400,
                                 val errorMessage: String = "Not found") : SynchronousApiCall<T>(response){

    override fun synchronousEnqueue(callback: Callback<T>) {
        if(isNetworkError) callback.onFailure(this, IOException(errorMessage))
        else callback.onResponse(this,Response.error<T>(errorCode, ResponseBody.create(null, errorMessage)))
    }
}