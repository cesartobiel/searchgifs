package com.cesarcardenas.giphysearch.common

import com.cesarcardenas.giphysearch.data.services.giphy.*
import com.cesarcardenas.giphysearch.domain.entities.GifEntitiy

fun generateGyphyResponse(numberOfGifs: Int = 5): GiphyResponse {
    val gifs = ArrayList<Data>()

    for(i in 1..numberOfGifs){
        gifs.add(
            Data(i.toString(), "Gif$i", generateRandomImagesResponseObject())
        )
    }

    return GiphyResponse(gifs, Pagination(numberOfGifs,0,numberOfGifs))
}

fun generateRandomImagesResponseObject(): Images {
    return Images(
        FixedHeightDownSampled("www.google.com"),
        FixedHeightSmall("www.google.com")
    )
}

fun generateGifEntities(number: Int = 5): List<GifEntitiy> {
    val gifs = ArrayList<GifEntitiy>()

    for(i in 1..number){
        gifs.add(GifEntitiy("Hero$i","www.heroes.com/hero$i","www.heroes.com/hero$i"))
    }

    return gifs
}

