package com.cesarcardenas.giphysearch.common

import org.mockito.junit.MockitoJUnit
import org.junit.Rule
import org.mockito.junit.MockitoRule


interface MockitoTest{
    @Rule fun mockitoRule():MockitoRule = MockitoJUnit.rule()
}