package com.cesarcardenas.giphysearch.common

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import org.junit.Rule
import org.junit.rules.TestRule

interface LiveDataTest{
    @Rule fun instantTaskExecutorRule(): TestRule = InstantTaskExecutorRule()
}