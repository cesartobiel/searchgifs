package com.cesarcardenas.giphysearch.data.datastores

import com.cesarcardenas.giphysearch.BuildConfig
import com.cesarcardenas.giphysearch.common.LiveDataTest
import com.cesarcardenas.giphysearch.common.MockitoTest
import com.cesarcardenas.giphysearch.common.api.FaileApiCall
import com.cesarcardenas.giphysearch.common.api.SuccessFulApiCall
import com.cesarcardenas.giphysearch.common.generateGyphyResponse
import com.cesarcardenas.giphysearch.data.services.RequestResult
import com.cesarcardenas.giphysearch.data.services.giphy.GiphyService
import com.cesarcardenas.giphysearch.data.services.giphy.GiphyResponse
import com.jraska.livedata.test
import org.junit.Assert
import org.junit.Test
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.Mockito.`when`
import org.mockito.stubbing.OngoingStubbing
import retrofit2.Call

class GiphyRemoteStoreTests: LiveDataTest, MockitoTest{

    @Mock lateinit var giphyService: GiphyService

    @InjectMocks lateinit var giphyRemoteStore : GiphyRemoteStore

    @Test
    fun `Api call succeeded, returns Success`() {
        //Arrange
        val numberOfGifs = 10
        val response = generateGyphyResponse(numberOfGifs)
        val search = "heroes"
        makeServiceReactToSearch(search).thenReturn(SuccessFulApiCall(response))

        //Act
        val testObserver = giphyRemoteStore.getGifsWith(search).test()

        //Assert
        val result = testObserver.assertHistorySize(1).value() as RequestResult.Success
        Assert.assertEquals(numberOfGifs,result.gifsData.size)
    }



    @Test
    fun `Api call failed(403), returns error result`() {
        //Arrange
        val response = generateGyphyResponse(10)
        val search = "heroes"
        val errorCode = 403
        val errorMessage = "Permission denied"
        makeServiceReactToSearch(search).thenReturn(FaileApiCall(response, false, errorCode, errorMessage))

        //Act
        val testObserver = giphyRemoteStore.getGifsWith(search).test()

        //Assert
        val error = testObserver.assertHistorySize(1).value() as RequestResult.Error
        Assert.assertEquals(errorCode,error.code)
        Assert.assertEquals(errorMessage,error.errorMessage)
    }

    @Test
    fun `Api call failed(Network Issues), returns error result`() {
        //Arrange
        val response = generateGyphyResponse(10)
        val search = "heroes"
        val errorCode = 0
        val errorMessage = "Unable to reach server"
        makeServiceReactToSearch(search).thenReturn(FaileApiCall(response, true, errorCode, errorMessage))
        //Act
        val testObserver = giphyRemoteStore.getGifsWith(search).test()

        //Assert
        val error = testObserver.assertHistorySize(1).value() as RequestResult.Error
        Assert.assertEquals(errorCode,error.code)
        Assert.assertNotNull(errorMessage,error.errorMessage)
    }

    //region Helper functions
    private fun makeServiceReactToSearch(search: String, response: GiphyResponse) {
        `when`(
            giphyService.search(
                BuildConfig.GiphyApiKey,
                giphyService.defaultLimit,
                search,
                giphyService.defaultoffset,
                giphyService.defaultRating,
                giphyService.defaultLanguage
            )
        )
            .thenReturn(SuccessFulApiCall(response))
    }

    private fun makeServiceReactToSearch(search: String): OngoingStubbing<Call<GiphyResponse>> {
        return `when`(
            giphyService.search(
                BuildConfig.GiphyApiKey,
                giphyService.defaultLimit,
                search,
                giphyService.defaultoffset,
                giphyService.defaultRating,
                giphyService.defaultLanguage
            )
        )
    }
    //endregion
}