package com.cesarcardenas.giphysearch.data.repositories

import com.cesarcardenas.giphysearch.common.LiveDataTest
import com.cesarcardenas.giphysearch.common.MockitoTest
import com.cesarcardenas.giphysearch.common.generateGyphyResponse
import com.cesarcardenas.giphysearch.data.datastores.GifsRemoteStore
import com.cesarcardenas.giphysearch.data.services.RequestResult
import com.cesarcardenas.giphysearch.domain.entities.GifsResult
import com.jraska.livedata.test
import com.snakydesign.livedataextensions.liveDataOf
import junit.framework.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito.`when`

class RemoteGifsRespositoryTests : LiveDataTest, MockitoTest{

   @Mock lateinit var gifsRemoteStore : GifsRemoteStore
    lateinit var remoteGifsRepo :RemoteGifsRepository

    @Before
    fun setup(){
        remoteGifsRepo = RemoteGifsRepository(gifsRemoteStore)
    }

    @Test
    fun `Search Succeded, returns expected results`(){
        //Arrange
        val search = "heroes"
        val expectedNumberOfGifs = 5
        val response = RequestResult.Success(generateGyphyResponse(expectedNumberOfGifs).data)
        `when`(gifsRemoteStore.getGifsWith(search))
            .thenReturn(liveDataOf(response))

        //Act
        val result = remoteGifsRepo.getGifsWith(search).test().value() as GifsResult.Succesfull

        //Assert
        assertEquals(expectedNumberOfGifs, result.gifs.size)
    }

    @Test
    fun `Search Failed returns empty results`(){
        //Arrange
        val search = "heroes"
        val response = RequestResult.Error(403, "Permission denied")
        `when`(gifsRemoteStore.getGifsWith(search))
            .thenReturn(liveDataOf(response))

        //Act
        val result = remoteGifsRepo.getGifsWith(search).test().value() as GifsResult.Error

        //Assert
        assertEquals(403, result.code)
    }

}