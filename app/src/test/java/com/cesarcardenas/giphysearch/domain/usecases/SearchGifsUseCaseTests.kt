package com.cesarcardenas.giphysearch.domain.usecases

import android.net.Uri
import androidx.lifecycle.LiveData
import com.cesarcardenas.giphysearch.common.LiveDataTest
import com.cesarcardenas.giphysearch.common.generateGifEntities
import com.cesarcardenas.giphysearch.domain.GifsRepository
import com.cesarcardenas.giphysearch.domain.entities.GifEntitiy
import com.cesarcardenas.giphysearch.domain.entities.GifsResult
import com.jraska.livedata.TestObserver
import org.junit.Test
import org.mockito.Mockito.`when`
import org.mockito.Mockito.mock
import com.snakydesign.livedataextensions.*
import org.junit.Assert


class SearchGifsUseCaseTests : LiveDataTest {

    @Test
    fun searchGif() {
        //Arrange
        val SEARCH = "Heroes"
        val gifsRepository = mock(GifsRepository::class.java)
        val expectedNumberOfGifs = 5
        `when`(gifsRepository.getGifsWith(SEARCH))
            .thenReturn(liveDataOf(
                GifsResult.Succesfull(generateGifEntities(expectedNumberOfGifs)))
            )

        val searchGifsUseCase = SearchGifsUseCase(gifsRepository)

        //Act
        val testObserver = TestObserver.test(searchGifsUseCase.search(SEARCH))

        //Assert
        val resultList = testObserver.assertHistorySize(1).value() as GifsResult.Succesfull
        Assert.assertEquals(expectedNumberOfGifs, resultList.gifs.size)
    }
}
