package com.cesarcardenas.giphysearch.robots

import androidx.test.espresso.Espresso
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.*
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.platform.app.InstrumentationRegistry
import androidx.test.uiautomator.UiDevice
import com.cesarcardenas.giphysearch.R
import org.hamcrest.CoreMatchers.not
import org.hamcrest.Matchers
import android.os.IBinder
import android.view.View
import android.view.WindowManager
import androidx.test.espresso.Root
import org.hamcrest.Description
import org.hamcrest.TypeSafeMatcher


class GifSearchRobot{


    fun search(search: String): GifSearchRobot {
        //Perform search
        onView(withId(R.id.searchView)).perform(typeText(search))
        val mDevice = UiDevice.getInstance(InstrumentationRegistry.getInstrumentation())
        mDevice.pressEnter()
        Thread.sleep(1000)
        return this
    }

    fun verifyNoResults(): GifSearchRobot {
        onView(withText(R.string.no_results)).inRoot(ToastMatcher()).check(matches(isDisplayed()))
        return this

    }

    fun verifyHasResults(): GifSearchRobot {
        onView(withId(R.id.gifsList)).check(matches(hasMinimumChildCount(1)))
        return this
    }

    fun clearSearch(): GifSearchRobot {
        // Clear the text in search field
        onView(Matchers.allOf(
            withContentDescription("Clear query"),
            isDisplayed()))
            .perform(click())

        return this
    }

    inner class ToastMatcher : TypeSafeMatcher<Root>() {

        override fun describeTo(description: Description) {
            description.appendText("is toast")
        }

        override fun matchesSafely(root: Root): Boolean {
            return when(root.decorView.visibility){
                View.VISIBLE  -> true
                else -> false
            }
        }
    }


}