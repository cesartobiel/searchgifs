package com.cesarcardenas.giphysearch

import androidx.test.rule.ActivityTestRule
import com.cesarcardenas.giphysearch.presentation.MainActivity
import com.cesarcardenas.giphysearch.robots.GifSearchRobot
import org.junit.Before
import org.junit.Rule
import org.junit.Test


class  GifsSearchTests{

    @get:Rule val activityRule = ActivityTestRule(MainActivity::class.java)
    lateinit var gifSearchRobot: GifSearchRobot

    @Before
    fun setUp(){
        gifSearchRobot = GifSearchRobot()
    }

    @Test
    fun searchByValidWord(){
        gifSearchRobot.search("Luffy")
            .verifyHasResults()
    }

    @Test
    fun searchByInValidWord(){
        gifSearchRobot.search("%^%^&*&")
            .verifyNoResults()
    }

    @Test
    fun searchByValidWordThenInvalidWord(){
        gifSearchRobot.search("Luffy")
            .verifyHasResults()
            .clearSearch()
            .search("$%^%^&*&")
            .verifyNoResults()
    }

}